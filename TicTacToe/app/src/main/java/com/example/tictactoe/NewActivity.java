package com.example.tictactoe;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.graphics.Color;

import android.view.View;
import android.widget.Button;
import android.os.Bundle;
import android.widget.TextView;
import android.content.Intent;

import androidx.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.util.HashMap;
import java.util.Map;

public class NewActivity extends AppCompatActivity {
    private NewTicViewModel ticViewModel;
    private Button one, two, three, four, five, six, seven, eight, nine, backButt;
    private TextView textView;
    private TextView gameIDTextView, playerStatusText, hostStatus;
    public boolean gameStarted; //used to track if a player leaves in loadGame

   // public Boolean beginning;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        String gameID = intent.getExtras().getString("gameID");
        String gamertag = intent.getExtras().getString("gamertag");

        Log.d("main", "gameID: "+gameID);
        Log.d("main", "gamertag: "+gamertag);

        ticViewModel = new ViewModelProvider(this, //create ViewModel
                new ViewModelProvider.NewInstanceFactory())
                .get(NewTicViewModel.class);

        // Access a Cloud Firestore instance from your Activity
        final FirebaseFirestore db = FirebaseFirestore.getInstance();

        ticViewModel.gameID.setValue(gameID); //save gameID
        ticViewModel.host.setValue(gamertag); //save host gamertag since this is new game
        Log.d("ViewModel", "host is "+ticViewModel.host.getValue());
        Log.d("local", "host is "+gamertag);

        one = (Button) findViewById(R.id.one);
        two = (Button) findViewById(R.id.two);
        three = (Button) findViewById(R.id.three);
        four = (Button) findViewById(R.id.four);
        five = (Button) findViewById(R.id.five);
        six = (Button) findViewById(R.id.six);
        seven = (Button) findViewById(R.id.seven);
        eight = (Button) findViewById(R.id.eight);
        nine = (Button) findViewById(R.id.nine);
        backButt = (Button) findViewById(R.id.backButt);
        gameIDTextView = (TextView) findViewById(R.id.gameIDTextView);
        textView = (TextView) findViewById(R.id.textView);
        hostStatus = (TextView) findViewById(R.id.hostStatus);
        playerStatusText = (TextView) findViewById(R.id.playerStatusText);
        gameIDTextView.setText("Hello, "+ticViewModel.host.getValue()+", invite others to join with gameID: "+ticViewModel.gameID.getValue()); //set the game ID textView
       // playerStatusText.setText("You are the only player in this lobby. Others can join with gameID: "+ticViewModel.gameID.getValue()); //invite others to join the game
        hostStatus.setText("Host");
        Map<String, Object> game = new HashMap<>();
        game.put("one", "");
        game.put("two", "");
        game.put("three", "");
        game.put("four", "");
        game.put("five", "");
        game.put("six", "");
        game.put("seven", "");
        game.put("eight", "");
        game.put("nine", "");
        game.put("current", "X");
        game.put("winner", "0");
        game.put("host", ticViewModel.host.getValue()); //saves your gamertag, the host
        game.put("other", ""); //saves the gamertag of the other player


        //creates new document for our game in database
        db.collection("games").document(ticViewModel.gameID.getValue().toString())
                .set(game)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("main", "DocumentSnapshot successfully written!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("main", "Error writing document", e);
                    }
                });


        //database listener, when it gets new information, it calls loadGame
        final DocumentReference docRef = db.collection("games").document(ticViewModel.gameID.getValue().toString());
        docRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot snapshot,
                                @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w("main", "Listen failed.", e);
                    return;
                }

                if (snapshot != null && snapshot.exists()) {
                    Log.d("main", "Current data: " + snapshot.getData());
                    loadGame(snapshot); //pull game state from data base
                } else {
                    Log.d("main", "Current data: null");
                }
            }
        });


//        ticViewModel.playerTwo().observe(this, new Observer<String>() {
//            @Override
//            public void onChanged(String player2) {
//                if(ticViewModel.player2.getValue() != "")
//                    playerTwoView.setText("You are playing with "+ticViewModel.otherPlayer());
//            }
//        });


        one.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (myTurn() && (one.getText() == String.valueOf("")) && !ticViewModel.ended.getValue()) {
                    one.setText(String.valueOf(ticViewModel.current.getValue())); //makes the requested move
                    if (isGameOver()) {//checks if this move has created a winner
                        saveGame(db, ticViewModel.gameID.getValue());
                        endingGame();
                    } else {
                        ticViewModel.nextPlayer(); //game continues, switch players
                        saveGame(db, ticViewModel.gameID.getValue());
                    }
                }
            }
        });
        two.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (myTurn() && (two.getText() == String.valueOf("")) && !ticViewModel.ended.getValue()) {
                    two.setText(String.valueOf(ticViewModel.current.getValue())); //makes the requested move
                    if (isGameOver()) {//checks if this move has created a winner
                        saveGame(db, ticViewModel.gameID.getValue());
                        endingGame();
                    } else {
                        ticViewModel.nextPlayer(); //game continues, switch players
                        saveGame(db, ticViewModel.gameID.getValue());
                    }
                }
            }
        });
        three.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (myTurn() && (three.getText() == String.valueOf("")) && !ticViewModel.ended.getValue()) {
                    three.setText(String.valueOf(ticViewModel.current.getValue())); //makes the requested move
                    if (isGameOver()) {//checks if this move has created a winner
                        saveGame(db, ticViewModel.gameID.getValue());
                        endingGame();
                    } else {
                        ticViewModel.nextPlayer(); //game continues, switch players
                        saveGame(db, ticViewModel.gameID.getValue());
                    }
                }
            }
        });
        four.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (myTurn() && (four.getText() == String.valueOf("")) && !ticViewModel.ended.getValue()) {
                    four.setText(String.valueOf(ticViewModel.current.getValue())); //makes the requested move
                    if (isGameOver()) {//checks if this move has created a winner
                        saveGame(db, ticViewModel.gameID.getValue());
                        endingGame();
                    } else {
                        ticViewModel.nextPlayer(); //game continues, switch players
                        saveGame(db, ticViewModel.gameID.getValue());
                    }
                }
            }
        });
        five.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (myTurn() && (five.getText() == String.valueOf("")) && !ticViewModel.ended.getValue()) {
                    five.setText(String.valueOf(ticViewModel.current.getValue())); //makes the requested move
                    if (isGameOver()) {//checks if this move has created a winner
                        saveGame(db, ticViewModel.gameID.getValue());
                        endingGame();
                    } else {
                        ticViewModel.nextPlayer(); //game continues, switch players
                        saveGame(db, ticViewModel.gameID.getValue());
                    }
                }
            }
        });
        six.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (myTurn() && (six.getText() == String.valueOf("")) && !ticViewModel.ended.getValue()) {
                    six.setText(String.valueOf(ticViewModel.current.getValue())); //makes the requested move
                    if (isGameOver()) {//checks if this move has created a winner
                        saveGame(db, ticViewModel.gameID.getValue());
                        endingGame();
                    } else {
                        ticViewModel.nextPlayer(); //game continues, switch players
                        saveGame(db, ticViewModel.gameID.getValue());
                    }
                }
            }
        });
        seven.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (myTurn() && (seven.getText() == String.valueOf("")) && !ticViewModel.ended.getValue()) {
                    seven.setText(String.valueOf(ticViewModel.current.getValue())); //makes the requested move
                    if (isGameOver()) {//checks if this move has created a winner
                        saveGame(db, ticViewModel.gameID.getValue());
                        endingGame();
                    } else {
                        ticViewModel.nextPlayer(); //game continues, switch players
                        saveGame(db, ticViewModel.gameID.getValue());
                    }
                }
            }
        });
        eight.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (myTurn() && (eight.getText() == String.valueOf("")) && !ticViewModel.ended.getValue()) {
                    eight.setText(String.valueOf(ticViewModel.current.getValue())); //makes the requested move
                    if (isGameOver()) {//checks if this move has created a winner
                        saveGame(db, ticViewModel.gameID.getValue());
                        endingGame();
                    } else {
                        ticViewModel.nextPlayer(); //game continues, switch players
                        saveGame(db, ticViewModel.gameID.getValue());
                    }
                }
            }
        });
        nine.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (myTurn() && (nine.getText() == String.valueOf("")) && !ticViewModel.ended.getValue()) {
                    nine.setText(String.valueOf(ticViewModel.current.getValue())); //makes the requested move
                    if (isGameOver()) {//checks if this move has created a winner
                        saveGame(db, ticViewModel.gameID.getValue());
                        endingGame();
                    } else {
                        ticViewModel.nextPlayer(); //game continues, switch players
                        saveGame(db, ticViewModel.gameID.getValue());
                    }
                }
            }
        });
        backButt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NewActivity.this, StartActivity.class);
                startActivity(intent); //open new acitivty/join existing game
            }
        });
    }

    public boolean myTurn(){
        if(ticViewModel.current.getValue().toString().equals("X") && gameReady())
            return true;
        else if (ticViewModel.current.getValue().toString().equals("O") && gameReady())
            return false;
        else
            return false;
    }

    public void saveGame(FirebaseFirestore db, String gameID){
        DocumentReference gameRef = db.collection("games").document(gameID);
        gameRef.update("ended", ticViewModel.ended.getValue());
        gameRef.update("one", one.getText());
        gameRef.update("two", two.getText());
        gameRef.update("two", two.getText());
        gameRef.update("three", three.getText());
        gameRef.update("four", four.getText());
        gameRef.update("five", five.getText());
        gameRef.update("six", six.getText());
        gameRef.update("seven", seven.getText());
        gameRef.update("eight", eight.getText());
        gameRef.update("nine", nine.getText());
        gameRef.update("current", ticViewModel.current.getValue().toString()); //last move value, save b4 switching players
        gameRef.update("winner", String.valueOf(ticViewModel.winner.getValue()));
    }

    public void loadGame(DocumentSnapshot snapshot){ //changes happen instantly because listener
        String other = snapshot.getString("other");
        ticViewModel.other.setValue(snapshot.getString("other")); //save the other's name
        if(gameReady()) { //both players in the game
            gameStarted=true;
            ticViewModel.other.setValue(snapshot.getString("other")); //save the other's name
            ticViewModel.current.setValue(snapshot.getString("current").charAt(0)); //save the last move
            gameIDTextView.setText("Hello, "+ticViewModel.host.getValue()+", you are playing with "+
                    ticViewModel.other.getValue()+
                    " in "+ticViewModel.gameID.getValue());
            if (ticViewModel.current.getValue().toString().equals("O"))
                playerStatusText.setText("It is " + other + "'s turn.");
            else if (ticViewModel.current.getValue().toString().equals("X"))
                playerStatusText.setText("It is your turn");
        }else if(gameStarted){
            Intent intent = new Intent(NewActivity.this, StartActivity.class);
            startActivity(intent); //open new acitivty/join existing game
        }

        one.setText(snapshot.getString("one"));
        two.setText(snapshot.getString("two"));
        three.setText(snapshot.getString("three"));
        four.setText(snapshot.getString("four"));
        five.setText(snapshot.getString("five"));
        six.setText(snapshot.getString("six"));
        seven.setText(snapshot.getString("seven"));
        eight.setText(snapshot.getString("eight"));
        nine.setText(snapshot.getString("nine"));


        //detecting winners
        switch (Integer.parseInt(snapshot.getString("winner"))) {
            case 0:
                break;
            case 1:
                if(myTurn())
                    textView.setText("Game Over! " + ticViewModel.host.getValue() + " wins!");
                else
                    textView.setText("Game Over! " + ticViewModel.other.getValue() + " wins!");
                one.setTextColor(Color.RED);
                two.setTextColor(Color.RED);
                three.setTextColor(Color.RED);
                Log.d("main", "1");
                break;
            case 2:
                if(myTurn())
                    textView.setText("Game Over! " + ticViewModel.host.getValue() + " wins!");
                else
                    textView.setText("Game Over! " + ticViewModel.other.getValue() + " wins!");
                four.setTextColor(Color.RED);
                five.setTextColor(Color.RED);
                six.setTextColor(Color.RED);
                Log.d("main", "2");
                break;
            case 3:
                if(myTurn())
                    textView.setText("Game Over! " + ticViewModel.host.getValue() + " wins!");
                else
                    textView.setText("Game Over! " + ticViewModel.other.getValue() + " wins!");
                seven.setTextColor(Color.RED);
                eight.setTextColor(Color.RED);
                nine.setTextColor(Color.RED);
                Log.d("main", "3");
                break;
            case 4:
                if(myTurn())
                    textView.setText("Game Over! " + ticViewModel.host.getValue() + " wins!");
                else
                    textView.setText("Game Over! " + ticViewModel.other.getValue() + " wins!");
                one.setTextColor(Color.RED);
                four.setTextColor(Color.RED);
                seven.setTextColor(Color.RED);
                Log.d("main", "4");
                break;
            case 5:
                if(myTurn())
                    textView.setText("Game Over! " + ticViewModel.host.getValue() + " wins!");
                else
                    textView.setText("Game Over! " + ticViewModel.other.getValue() + " wins!");
                two.setTextColor(Color.RED);
                five.setTextColor(Color.RED);
                eight.setTextColor(Color.RED);
                Log.d("main", "5");
                break;
            case 6:
                if(myTurn())
                    textView.setText("Game Over! " + ticViewModel.host.getValue() + " wins!");
                else
                    textView.setText("Game Over! " + ticViewModel.other.getValue() + " wins!");
                three.setTextColor(Color.RED);
                six.setTextColor(Color.RED);
                nine.setTextColor(Color.RED);
                Log.d("main", "6");
                break;
            case 7:
                if(myTurn())
                    textView.setText("Game Over! " + ticViewModel.host.getValue() + " wins!");
                else
                    textView.setText("Game Over! " + ticViewModel.other.getValue() + " wins!");
                one.setTextColor(Color.RED);
                five.setTextColor(Color.RED);
                nine.setTextColor(Color.RED);
                Log.d("main", "7");
                break;
            case 8:
                if(myTurn())
                    textView.setText("Game Over! " + ticViewModel.host.getValue() + " wins!");
                else
                    textView.setText("Game Over! " + ticViewModel.other.getValue() + " wins!");
                three.setTextColor(Color.RED);
                five.setTextColor(Color.RED);
                seven.setTextColor(Color.RED);
                Log.d("main", "8");
                break;
            case 9:
                textView.setText("Tie Game!");
                break;
        }
    }

    public boolean gameReady(){
        String host = ticViewModel.host.getValue();
        String other = ticViewModel.other.getValue();
        if(host != "" && other != "")
            return true;
        else
            return false;
    }

    public void endingGame(){
        ticViewModel.endGame();
    }



    public boolean isGameOver(){
        //row 1
        if((one.getText().equals(String.valueOf('X')) &&
                two.getText().equals(String.valueOf('X')) &&
                three.getText().equals(String.valueOf('X'))) ||
                (one.getText().equals(String.valueOf('O')) &&
                        two.getText().equals(String.valueOf('O')) &&
                        three.getText().equals(String.valueOf('O'))))
                {
                    ticViewModel.winnerSet(1);
                    return true;
        }
        //row 2
        else if((four.getText().equals(String.valueOf('X')) &&
                five.getText().equals(String.valueOf('X')) &&
                six.getText().equals(String.valueOf('X'))) ||
                (four.getText().equals(String.valueOf('O')) &&
                        five.getText().equals(String.valueOf('O')) &&
                        six.getText().equals(String.valueOf('O'))))
                {
                    ticViewModel.winnerSet(2);
            return true;
        }
        //row 3
        else if((seven.getText().equals(String.valueOf('X')) &&
                eight.getText().equals(String.valueOf('X')) &&
                nine.getText().equals(String.valueOf('X'))) ||
                (seven.getText().equals(String.valueOf('O')) &&
                        eight.getText().equals(String.valueOf('O')) &&
                        nine.getText().equals(String.valueOf('O'))))
                {
                    ticViewModel.winnerSet(3);
            return true;
        }
        //col 1
        else if((one.getText().equals(String.valueOf('X')) &&
                four.getText().equals(String.valueOf('X')) &&
                seven.getText().equals(String.valueOf('X'))) ||
                (one.getText().equals(String.valueOf('O')) &&
                        four.getText().equals(String.valueOf('O')) &&
                        seven.getText().equals(String.valueOf('O'))))
        {
            ticViewModel.winnerSet(4);
            return true;
        }
        //col 2
        else if((two.getText().equals(String.valueOf('X')) &&
                five.getText().equals(String.valueOf('X')) &&
                eight.getText().equals(String.valueOf('X'))) ||
                (two.getText().equals(String.valueOf('O')) &&
                        five.getText().equals(String.valueOf('O')) &&
                        eight.getText().equals(String.valueOf('O'))))
        {
            ticViewModel.winnerSet(5);
            return true;
        }
        //col 3
        else if((three.getText().equals(String.valueOf('X')) &&
                six.getText().equals(String.valueOf('X')) &&
                nine.getText().equals(String.valueOf('X'))) ||
                (three.getText().equals(String.valueOf('O')) &&
                        six.getText().equals(String.valueOf('O')) &&
                        nine.getText().equals(String.valueOf('O'))))
        {
            ticViewModel.winnerSet(6);
            return true;
        }
        //diaganol top left bottom right
        else if((one.getText().equals(String.valueOf('X')) &&
                five.getText().equals(String.valueOf('X')) &&
                nine.getText().equals(String.valueOf('X'))) ||
                (one.getText().equals(String.valueOf('O')) &&
                        five.getText().equals(String.valueOf('O')) &&
                        nine.getText().equals(String.valueOf('O'))))
        {
            ticViewModel.winnerSet(7);
            return true;
        }
        //diaganol top right bottom left
        else if((three.getText().equals(String.valueOf('X')) &&
                five.getText().equals(String.valueOf('X')) &&
                seven.getText().equals(String.valueOf('X'))) ||
                (three.getText().equals(String.valueOf('O')) &&
                        five.getText().equals(String.valueOf('O')) &&
                        seven.getText().equals(String.valueOf('O'))))
        {
            ticViewModel.winnerSet(8);
            return true;
        }
        else if(tie()){
            ticViewModel.winnerSet(9);
            return true;
        }
        else{
            ticViewModel.winnerSet(0);
            return false;
        }
    }

    public boolean tie(){
        if(!String.valueOf(one.getText()).isEmpty() &&
        !String.valueOf(two.getText()).isEmpty() &&
        !String.valueOf(three.getText()).isEmpty() &&
        !String.valueOf(four.getText()).isEmpty() &&
        !String.valueOf(five.getText()).isEmpty() &&
        !String.valueOf(six.getText()).isEmpty() &&
        !String.valueOf(seven.getText()).isEmpty() &&
        !String.valueOf(eight.getText()).isEmpty() &&
            !String.valueOf(one.getText()).isEmpty())
        {
            return true;
        }
        else
            return false;
    }
}