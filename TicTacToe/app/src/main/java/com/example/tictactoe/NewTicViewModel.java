package com.example.tictactoe;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


import android.os.SystemClock;
import android.util.Log;

public class NewTicViewModel extends ViewModel{
    public MutableLiveData<Boolean> ended = new MutableLiveData<>(false);
    public MutableLiveData<Character> current = new MutableLiveData<>('X');
    public MutableLiveData<String> gameID = new MutableLiveData<>("");
    public MutableLiveData<String> host = new MutableLiveData<>("");
    public MutableLiveData<String> other = new MutableLiveData<>("");
    public MutableLiveData<Integer> winner  = new MutableLiveData<>();


    public void nextPlayer() {
        if(current.getValue() == 'X')
            current.setValue('O');
        else
            current.setValue('X');
    }
    public void endGame() {
        ended.setValue(true);
    }

    public void restart() {
        ended.setValue(false);
        current.setValue('X');
    }
    public void winnerSet(Integer i){
        winner.setValue(i);
    }
}
