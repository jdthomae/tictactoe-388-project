package com.example.tictactoe;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class StartActivity extends AppCompatActivity {
    Button createButt, joinButt;
    EditText existGameID, newGameID, existGamertag, newGamertag;
    TextView statusTextView;
    public static final String NO = "com.example.myfirstapp.MESSAGE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        createButt = (Button) findViewById(R.id.createButt);
        joinButt = (Button) findViewById(R.id.joinButt);
        newGamertag = (EditText) findViewById(R.id.newGamertag);
        existGamertag = (EditText) findViewById(R.id.existGamertag);
        newGameID = (EditText) findViewById(R.id.newGameID);
        existGameID = (EditText) findViewById(R.id.existGameID);
        statusTextView = (TextView) findViewById(R.id.statusTextView);
        final FirebaseFirestore db = FirebaseFirestore.getInstance();

        joinButt.setOnClickListener(new View.OnClickListener(){ //user is trying to join a game
            @Override
            public void onClick(View view) {
                db.collection("games").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        String gameID = existGameID.getText().toString();
                        String gamertag = existGamertag.getText().toString();
                        if (task.isSuccessful()) {
                            List<String> list = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                list.add(document.getId()); //list of gameIDs from db
                            }
                            if(gameInCollection(list, gameID)){ //check the db for the existing game
                                statusTextView.setText("Joining '" + gameID + "'...");
                                Intent intent = new Intent(StartActivity.this, ExistActivity.class);
                                intent.putExtra("gameID", gameID);
                                intent.putExtra("gamertag", gamertag);
                                startActivity(intent); //open new acitivty/join existinging game
                            } else {
                                statusTextView.setText("We couldn't find the game: '" + gameID + "', try again"); //error message if game doesn't exist
                            }
                        } else {
                            Log.d("Start", "Error getting documents: ", task.getException());
                        }
                    }
                });
            }
        });

        createButt.setOnClickListener(new View.OnClickListener(){ //user is trying to join a game
            @Override
            public void onClick(View view) {
                db.collection("games").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        String gameID = newGameID.getText().toString();
                        String gamertag = newGamertag.getText().toString();
                        if (task.isSuccessful()) {
                            List<String> list = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                list.add(document.getId()); //list of gameIDs from db
                            }
                            if(gameInCollection(list, gameID)){ //check the db for the existing game
                                statusTextView.setText("The game, '" + gameID + "', already exists, try again"); //error message if exists
                            } else {
                                statusTextView.setText("Creating game: '" + gameID + "'...");
                                Intent intent = new Intent(StartActivity.this, NewActivity.class);
                                intent.putExtra("gameID", gameID);
                                intent.putExtra("gamertag", gamertag);
                                startActivity(intent); //open new acitivty/join existing game
                            }
                        } else {
                            Log.d("Start", "Error getting documents: ", task.getException());
                        }
                    }
                });
            }
        });

    }
    public boolean gameInCollection(List<String> list, String game){
        for(int i = 0; i < list.size(); i++){
            if(list.get(i).equals(game)) {
                return true;
            }
        }
        return false;
    }

}


//                            for(int i = 0; i < list.size(); i++){
//                                Log.d("Start", list.get(i));
//                                if(list.get(i).equals(game)) { //check if the gameID entered is in the list
//                                    Log.d("start", "'" + game + "'is in here");
//                                    Intent intent = new Intent(StartActivity.this, MainActivity.class);
//                                    intent.putExtra("gameID", game);
//                                    intent.putExtra("gamertag", player1);
//                                    startActivity(intent);
//}